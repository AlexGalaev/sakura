//
//  File.swift
//  SakuraReload
//
//  Created by Alexey.IOS on 06.07.15.
//  Copyright (c) 2015 SpbDev. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireObjectMapper
import AlecrimCoreData

typealias AutoCompleteResponce = ([String]?, NSError?) -> Void

typealias OrderResponce = (Bool) -> Void

enum NetworkError: ErrorType {
    case NoNetwork
    case NoData
}

class NetworkManager : Alamofire.Manager{
    
//    let url = "http://93.92.194.154:8111/saktest/hs/courier/update/112233"
//    func requestPost(url: String) {
//    
//        Alamofire.request(.POST, url).authenticate(user:"geosite", password:"123")
//        .response
//        {
//            (request, response, JSON, error) in
//             println(response)
//             println(JSON)
//        }
//    }
    
    func deleteOldData(dataType:DataType)
    {
        switch(dataType){
        case .kDataAll:
            
            do {
                try dataContext.offers.delete()
                try dataContext.dishes.delete()
                try dataContext.kitchenCatalogs.delete()
                try dataContext.addSauses.delete()
                try dataContext.meatBases.delete()
                try dataContext.news.delete()
                try dataContext.noodlesBases.delete()
                try dataContext.pizzaToppings.delete()
                try dataContext.sauses.delete()
                try dataContext.toppings.delete()
                try dataContext.orders.delete()
                try dataContext.orderToppings.delete()
                } catch {
                print("Error")
            }
         break
        case .kDataCities:
            do {
            try dataContext.cities.delete()
            } catch {
            }
        default:
            break
        }
        do {
            try dataContext.cities.delete()
        } catch {
            
        }
    }
    func loadData(dataType:DataType) {
        switch(dataType){
            case .kDataAll:
                print ("Xuy tebe")
//                getJapMenuList(Settings.japMenuUrl)
//
//                getPanMenuList(Settings.panMenuUrl)
//
//                getItalMenuList(Settings.itaMenuUrl)
//
//                getJapCatList(Settings.japCatUrl)
//                
//                getItalCatList(Settings.itaCatUrl)
//                
//                getPanCatList(Settings.panCatUrl)
//
//                getPizzaTop()
//                
//                getBaseNoodles()
//                
//                getBaseToppings()
//                
//                getBaseAddSouses()
//                
//                getBaseMeat()
//                
//                getNews()
//            
//                getOffers()
    
        default:
            break
        }
    }

    func getCityList(){
        Alamofire.request(.GET, Settings.CITIESURL, parameters:nil)
            .responseObject { (response: CityMapper?, error: ErrorType?) in
                if let cityes = response?.cities {
                    for city in cityes {
                        let newcity = dataContext.cities.createEntity()
                        newcity.id = (city.cityId?.intValue)!
                        newcity.phone = city.phone
                        newcity.name = city.name
                    }
                    do { try dataContext.save()
                        print("CityOK\(response?.status)")
                        NSNotificationCenter.defaultCenter().postNotificationName(Settings.kCityListDone,object: nil)
                    } catch {
                        NSNotificationCenter.defaultCenter().postNotificationName(Settings.kNetworkManagerDidEndWithError, object:"DataBaseError")
                    }
                }
                else
                {
                    NSNotificationCenter.defaultCenter().postNotificationName(Settings.kNetworkManagerDidEndWithError, object:"NetworkError")
                }
        }
    }

//    func getJapMenuList(url:String) {
//    Alamofire.request(.GET, url)
//            .responseObject { (response: KitchenCatMapper?, error: ErrorType?) in
//            //    println(error)
//                if let japmenu = response?.japmenu {
//                        for menu in japmenu
//                    {
//                        let newmenu = dataContext.kitchenCatalogs.createEntity()
//                        newmenu.menuType = String(format:"%@",(response?.action)!)
//                        newmenu.catalogId = (menu.menuId?.intValue)!
//                        newmenu.name = menu.name
//                        newmenu.photo = menu.photo
//                    }
//                    let (success, error) = dataContext.save()
//                    if success {
//                        println("japMenu ok = \(response?.status)")
//                        NSNotificationCenter.defaultCenter().postNotificationName(Settings.kNetworkManagerDidEnd, object: nil)
//                    }
//                }
//                    else {
//                             NSNotificationCenter.defaultCenter().postNotificationName(Settings.kNetworkManagerDidEndWithError,object:error)
//            }
//                
//        }
//
//    }
// //3
//    func getPanMenuList(url:String) {
//        
//        Alamofire.request(.GET, url)
//            .responseObject { (response: KitchenCatMapper?, error: ErrorType?) in
//           //      println(error)
//                if let panmenus = response?.panmenu {
//                    for menu in panmenus
//                    {
//                        let newmenu = dataContext.kitchenCatalogs.createEntity()
//                        newmenu.menuType = response?.action
//                        newmenu.catalogId = menu.menuId?.integerValue
//                        newmenu.name = menu.name
//                        newmenu.photo = menu.photo
//                    }
//                        let (success, error) = dataContext.save()
//                            if success {
//                                 println("PanMenu ok = \(response?.status)")
//                                NSNotificationCenter.defaultCenter().postNotificationName(Settings.kNetworkManagerDidEnd, object: nil)
//                        }
//                    
//                    } else {
//                    
//                          NSNotificationCenter.defaultCenter().postNotificationName(Settings.kNetworkManagerDidEndWithError,object: error)
//                    }
//                
//            }
//        
//    }
////4
//    func getItalMenuList(url:String) {
//        
//        Alamofire.request(.GET, url)
//            .responseObject { (response: KitchenCatMapper?, error: ErrorType?) in
//           //      println(error)
//                if let itmenu = response?.itmenu {
//                    for menu in itmenu
//                    {
//                        let newmenu = dataContext.kitchenCatalogs.createEntity()
//                        newmenu.menuType = response?.action
//                        newmenu.catalogId = menu.menuId?.integerValue
//                        newmenu.name = menu.name
//                        newmenu.photo = menu.photo
//                    }
//                    
//                    let (success, error) = dataContext.save()
//                    if success {
//                        println("ItalMenu ok = \(response?.status)")
//                        NSNotificationCenter.defaultCenter().postNotificationName(Settings.kNetworkManagerDidEnd, object: error)
//                        
//                    }
//                } else {
//                NSNotificationCenter.defaultCenter().postNotificationName(Settings.kNetworkManagerDidEndWithError,object: error)
//                }
//        }
//        
//    }
//    
//    func getJapCatList(url: String) {
//    
//        Alamofire.request(.GET, url)
//            .responseObject { (response: DishCat?, error: ErrorType?) in
//             //     println(error)
//                if let menus = response?.japcatalog{
//                for menu in menus
//                {
//                    let newdish = dataContext.dishes.createEntity()
//                    newdish.catalogName = response?.action
//                    newdish.categoryId = menu.categoryId?.integerValue
//                    newdish.dishId = menu.dishId?.integerValue
//                    newdish.sPhoto = menu.sPhoto
//                    newdish.lPhoto = menu.lPhoto
//                    newdish.infoLabel = menu.infoLabel
//                    newdish.ingredients = menu.ingredients
//                    newdish.price1 = menu.price1?.integerValue
//                    newdish.name = menu.name
//                }
//                let (success, error) = dataContext.save()
//                    if success {
//                    println("JApCat ok = \(response?.status)")
//                    NSNotificationCenter.defaultCenter().postNotificationName(Settings.kNetworkManagerDidEnd, object: nil)
//                    }
//                } else {
//                        NSNotificationCenter.defaultCenter().postNotificationName(Settings.kNetworkManagerDidEndWithError,object: error)
//                }
//        }
//    }
//    
//    func getPanCatList(url: String) {
//        Alamofire.request(.GET, url)
//            .responseObject { (response: DishCat?, error: ErrorType?) in
//             //    println(error)
//                if let  menus = response?.pancatalog
//                {
//                    for menu in menus
//                    {
//                        let newdish = dataContext.dishes.createEntity()
//                        newdish.catalogName = response?.action
//                        newdish.categoryId = (menu.categoryId?.intValue)!
//                        newdish.dishId = menu.dishId?.intValue
//                        newdish.sPhoto = menu.sPhoto
//                        newdish.lPhoto = menu.lPhoto
//                        newdish.infoLabel = menu.infoLabel
//                        newdish.ingredients = menu.ingredients
//                        newdish.price1 = menu.price1?.integerValue
//                        newdish.name = menu.name
//                    }
//                    let (success, error) = dataContext.save()
//                        if success {
//                        print("panCat ok = \(response?.status)")
//            //                NSNotificationCenter.defaultCenter().postNotificationName(Settings.kNetworkManagerDidEnd, object: nil)
//                        } else {
//                 //       NSNotificationCenter.defaultCenter().postNotificationName(Settings.kNetworkManagerDidEndWithError,object: error)
//                        }
//                }
//                else {
//            //            NSNotificationCenter.defaultCenter().postNotificationName(Settings.kNetworkManagerDidEndWithError,object: error)
//                }
//        }
//    }
//    
//    func getItalCatList(url: String) {
//        
//        Alamofire.request(.GET, url)
//            .responseObject { (response: DishCat?, error: ErrorType?) in
//              //   println(error)
//                if let menus = response?.itacatalog{
//                    for menu in menus
//                    {
//                        let newdish = dataContext.dishes.createEntity()
//                        newdish.catalogName = response?.action
//                        newdish.categoryId = menu.categoryId?.integerValue
//                        newdish.dishId = menu.dishId?.integerValue
//                        newdish.sPhoto = menu.sPhoto
//                        newdish.lPhoto = menu.lPhoto
//                        newdish.infoLabel = menu.infoLabel
//                        newdish.ingredients = menu.ingredients
//                        newdish.price1 = menu.price1?.integerValue
//                        newdish.name = menu.name
//                    }
//                    let (success, error) = dataContext.save()
//                    if success {
//                        println("italCat ok = \(response?.status)")
//                        NSNotificationCenter.defaultCenter().postNotificationName(Settings.kNetworkManagerDidEnd, object: nil)
//
//                    } else {
//                //    NSNotificationCenter.defaultCenter().postNotificationName(Settings.kNetworkManagerDidEndWithError,object: nil)
//                    }
//                }
//                else
//                {
//              //      NSNotificationCenter.defaultCenter().postNotificationName(Settings.kNetworkManagerDidEndWithError,object: error)
//                }
//        }
//    }
//    
//    func getOffers() {
//        
//        Alamofire.request(.GET, Settings.offerString)
//            .responseObject { (response: OfferMapper?, error: ErrorType?) in
//                println(error)
//                if let offers = response?.offers {
//                    for offer in offers {
//                        let newoffer = dataContext.offers.createEntity()
//                        newoffer.name = offer.name
//                        newoffer.date = offer.date
//                        newoffer.ldescr = offer.ldescr
//                        newoffer.sdescr = offer.sdescr
//                        newoffer.s_photo = offer.s_photo
//                        newoffer.lphoto =   offer.lphoto
//                        newoffer.offer_id = offer.offer_id?.integerValue
//                    }
//                    let (success, error) = dataContext.save()
//                        if success {
//                        println("offers ok = \(response?.status)")
//                        NSNotificationCenter.defaultCenter().postNotificationName(Settings.kNetworkManagerDidEnd, object: nil)
//                        } else {
//                    //    NSNotificationCenter.defaultCenter().postNotificationName(Settings.kNetworkManagerDidEndWithError,object: error)
//                    }
//                }
//                else
//                {
//         //           NSNotificationCenter.defaultCenter().postNotificationName(Settings.kNetworkManagerDidEndWithError,object: error)
//                }
//        }
//    }
//    
//    func getPizzaTop() {
//
//        Alamofire.request(.GET, Settings.pizzaTopUrl).responseObject { (response: PizzaToppings?, error: ErrorType?) in
//           //  println(error)
//            if let toppings = response?.pizzatoppings {
//                for topping in toppings {
//                    let newTopping = dataContext.pizzaTopings.createEntity()
//                    newTopping.toppingID = topping.pizatopId?.integerValue
//                    newTopping.name = topping.name
//                    newTopping.price = topping.price?.integerValue
//                    newTopping.weight = topping.weight?.integerValue
//                }
//                let (success, error) = dataContext.save()
//                if success {
//                    NSNotificationCenter.defaultCenter().postNotificationName(Settings.kNetworkManagerDidEnd, object: nil)
//                } else {
//            //    NSNotificationCenter.defaultCenter().postNotificationName(Settings.kNetworkManagerDidEndWithError,object: nil)
//                }
//            }
//            else
//            {
//              //  NSNotificationCenter.defaultCenter().postNotificationName(Settings.kNetworkManagerDidEndWithError,object: error)
//            }
//
//        }
//    }
//    
//    func getBaseNoodles() {
//        Alamofire.request(.GET, Settings.kbaseNoodlUrl).responseObject { (response:BaseNoodles?, error: ErrorType?) in
//            // println(error)
//            if let noodles = response?.noodles {
//                for noodle in noodles {
//                    let newnodl = dataContext.noodlesBases.createEntity()
//                    newnodl.baseID = noodle.bnID?.integerValue
//                    newnodl.name = noodle.name
//                    newnodl.price = response?.price?.integerValue
//                    let (success, error) = dataContext.save()
//                    if success {
//                        NSNotificationCenter.defaultCenter().postNotificationName(Settings.kNetworkManagerDidEnd, object: nil)
//                        
//                    } else {
//                 //   NSNotificationCenter.defaultCenter().postNotificationName(Settings.kNetworkManagerDidEndWithError,object:error)
//                    }
//                }
//            }
//            else {
//       //     NSNotificationCenter.defaultCenter().postNotificationName(Settings.kNetworkManagerDidEndWithError,object: error)
//            }
//        }
//    
//    }
//    
//    func getBaseToppings() {
//        Alamofire.request(.GET, Settings.kBASETOPUrl).responseObject { (response:BaseToppings?, error: ErrorType?) in
//            // println(error)
//            if let btoppings = response?.topings {
//                for toping in btoppings {
//                    let newnodl = dataContext.toppings.createEntity()
//                    newnodl.toppingId = toping.tID?.integerValue
//                    newnodl.name = toping.name
//            
//                    let (success, error) = dataContext.save()
//                    if success {
//                        NSNotificationCenter.defaultCenter().postNotificationName(Settings.kNetworkManagerDidEnd, object: nil)
//                    } else {
//             
//               //     NSNotificationCenter.defaultCenter().postNotificationName(Settings.kNetworkManagerDidEndWithError,object: error)
//                    }
//                }
//                
//            }
//            else
//            {
//              //  NSNotificationCenter.defaultCenter().postNotificationName(Settings.kNetworkManagerDidEndWithError,object: error)
//            }
//
//        }
//        
//    }
//    
//    func getBaseAddSouses() {
//        Alamofire.request(.GET, Settings.kBASEADDSAUSUrl).responseObject { (response:BaseAddSouses?, error: ErrorType?) in
//     
//            if let addsss = response?.addsss {
//                for ss in addsss {
//                    let newaddss = dataContext.addSauses.createEntity()
//                    newaddss.sauseID = ss.tID?.integerValue
//                    newaddss.name = ss.name
//                    newaddss.price = ss.price?.integerValue
//                    let (success, error) = dataContext.save()
//                    if success {
//                        NSNotificationCenter.defaultCenter().postNotificationName(Settings.kNetworkManagerDidEnd, object: nil)
//                    } else {
//                //    NSNotificationCenter.defaultCenter().postNotificationName(Settings.kNetworkManagerDidEndWithError,object: error)
//                    }
//                }
//                
//            }
//            else
//            {
//             //   NSNotificationCenter.defaultCenter().postNotificationName(Settings.kNetworkManagerDidEndWithError,object: error)
//            }
//
//        }
//        
//    }
//    
//    func getBaseMeat() {
//        Alamofire.request(.GET, Settings.kBASEMEATUrl).responseObject { (response:BaseAddMeat?, error: ErrorType?) in
//            //println(error)
//            if let addsss = response?.addsss {
//                for ss in addsss {
//                    let newMeat = dataContext.meatBases.createEntity()
//                    newMeat.meatID = ss.tID?.integerValue
//                    newMeat.name = ss.name
//                    newMeat.price = ss.price?.integerValue
//                    let (success, error) = dataContext.save()
//                    if(success){
//                        NSNotificationCenter.defaultCenter().postNotificationName(Settings.kNetworkManagerDidEnd, object: nil)
//                    } else {
//               //     NSNotificationCenter.defaultCenter().postNotificationName(Settings.kNetworkManagerDidEndWithError,object: error)
//                    }
//                }
//                
//            }
//            else
//            {
//               // NSNotificationCenter.defaultCenter().postNotificationName(Settings.kNetworkManagerDidEndWithError,object: error)
//            }
//
//        }
//        
//    }
//
//    func getNews(){
//        Alamofire.request(.GET, Settings.kNEWSUrl)
//            .responseObject { (response: NewsMapper?, error: ErrorType?) in
//                 //println(error)
//                
//                if let news = response?.news {
//                    for new in news {
//                        let newone = dataContext.news.createEntity()
//                        newone.name = new.name
//                        newone.date = new.date?.integerValue
//                        newone.ldescr = new.ldescr
//                        newone.sdescr = new.sdescr
//                        newone.sphoto = new.s_photo
//                        newone.lphoto =   new.lphoto
//                        newone.offersID = new.offer_id?.integerValue
//                    }
//                    let (success, error) = dataContext.save()
//                    if success {
//                        NSNotificationCenter.defaultCenter().postNotificationName(Settings.kNetworkManagerDidEnd, object: nil)
//                    }else {
//                  //  NSNotificationCenter.defaultCenter().postNotificationName(Settings.kNetworkManagerDidEndWithError,object: error)
//                    }
//                }
//                else
//                {
//                 //   NSNotificationCenter.defaultCenter().postNotificationName(Settings.kNetworkManagerDidEndWithError,object: error)
//                }
//
//            }
//        }
//    
//    
//    func getAutocompleteString(text:String, onCompletion:AutoCompleteResponce) {
//        var urlString = "\(Settings.baseURLString)?key=\(Settings.googleMapsKey)&input=\(text)"
//        if let url = NSURL(string: urlString.stringByAddingPercentEscapesUsingEncoding(NSUTF8StringEncoding)!){
//        Alamofire.request(.GET, url).responseJSON{(_, responce, JSON, error) in
//            if let dict = JSON as? NSDictionary {
//                let status = dict["status"] as? String
//                if status == "OK"{
//                    if let predictions = dict["predictions"] as? NSArray{
//                        var locations = [String]()
//                        for dict in predictions as! [NSDictionary]{
//                            locations.append(dict["description"] as! String)
//                        }
//                        onCompletion(locations,nil)
//                        }
//                    }
//                }
//            }
//        }
//    }
//    
//    
//    func sendOrder(string:String, resp:OrderResponce){
//        let urlString = "\(Settings.ORDER_URL)"
//            Alamofire.request(.POST, urlString, parameters: [:], encoding: .Custom({
//                (convertible, params) in
//                var mutableRequest = convertible.URLRequest.copy() as! NSMutableURLRequest
//                mutableRequest.HTTPBody = string.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false)
//                return (mutableRequest, nil)
//            })).response { (request, responce, data, error) -> Void in
//            if (responce?.statusCode == 200){
//                
//                    print (responce)
//                    resp(true)
//            } else {
//                    print (error)
//                    resp(false)
//                }
//            }
//            
//        }
}//End



//        if let url = NSURL(string: urlString.stringByAddingPercentEscapesUsingEncoding(NSUTF8StringEncoding)!){
