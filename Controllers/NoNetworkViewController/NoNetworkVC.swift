//
//  NoNetworkVC.swift
//  SakuraReload
//
//  Created by Alexey.IOS on 03.08.15.
//  Copyright (c) 2015 SpbDev. All rights reserved.
//

import UIKit

class NoNetworkVC: UIViewController {
   
    @IBOutlet weak var makeCallButton: UIButton!
    override func viewDidLoad()
    {
        super.viewDidLoad()
       
        if reachability.isReachable() {
            print(reachability.currentReachabilityString)
            self.dismissViewControllerAnimated(true, completion: nil)
        } else {
            print(reachability.currentReachabilityString)
        }
    }
    
    override func viewWillAppear(animated: Bool)
    {
        let callstring = "Заказать по телефону"
        makeCallButton.setTitle(callstring, forState:UIControlState.Normal)
    }
    
    @IBAction func makeCall(sender: AnyObject)
    {
        let phoneNumberUrl = NSURL(string:String(format:"tel:%d", 4588888))
        UIApplication.sharedApplication().openURL(phoneNumberUrl!)
    }

}
