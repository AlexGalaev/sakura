//
//  SplashVC.swift
//  SakuraReload
//
//  Created by Alexey.IOS on 20.07.15.
//  Copyright (c) 2015 SpbDev. All rights reserved.
//

import UIKit
import AlecrimCoreData


var errorString:NSString = ""

class SplashVC: UIViewController {
 
    @IBOutlet weak var sakuraindicator: UIImageView!
  
    var progress:Float = 0
    var isRotating = false
    var shouldStopRotating = false
   // @IBOutlet var prv: BallProgressView!
    
  
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBarHidden = true
   
        
    }

    func refresh() {
        if self.isRotating == false {
            self.sakuraindicator.rotate360Degrees(completionDelegate: self)
            self.isRotating = true
        }
    }
    
    override func animationDidStop(anim: CAAnimation, finished flag: Bool) {
        if self.shouldStopRotating == false {
            self.sakuraindicator.rotate360Degrees(completionDelegate: self)
        } else {
            self.reset()
        }
    }
    
    func reset() {
        self.isRotating = false
        self.shouldStopRotating = false
    }
    
    override func viewDidDisappear(animated: Bool) {
        NSNotificationCenter.defaultCenter().removeObserver(self, name:Settings.kCityListDone, object: nil)
        NSNotificationCenter.defaultCenter().removeObserver (self, name:Settings.kNetworkManagerDidEndWithError, object: self)
        NSNotificationCenter.defaultCenter().removeObserver(self, name:Settings.kNetworkManagerDidEnd, object: nil)
        progress = 1.2
        reset()
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    
    }
    
    override func viewWillAppear(animated: Bool) {
      //  prv.progressView = AGBallProgresView()
        super.viewWillAppear(true)
        NSNotificationCenter.defaultCenter().addObserver (self, selector:"dataUpdated" , name:Settings.kNetworkManagerDidEnd, object: nil)
        NSNotificationCenter.defaultCenter().addObserver (self, selector:"showCityList" , name:Settings.kCityListDone, object: nil)
        NSNotificationCenter.defaultCenter().addObserver (self, selector:"dataUpdatedWithError" , name:Settings.kNetworkManagerDidEndWithError, object: self)
        progress = 0
        reset()
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(true)
       
        self.validateDataOrUpdate()
        
    }
    
    func dataUpdated() {
        progressim()
        if(validateData()){
            myApp.showTabBar()
        }
    }
    
    func showCityList() {
            myApp.showCityList()
    }
    
    func dataUpdatedWithError(error:NSNotification){
        print("ERRRRRRRRROOR")
        errorString = error.description
//         var alertError = UIAlertController(title: "Error", message:errorString as String , preferredStyle: UIAlertControllerStyle.ActionSheet)
    }
    
    func validateDataOrUpdate(){
        if(Settings.cityID !== 0) {
           print("NeedstoUpdate = \(Settings.needUpdate())")
            print("cityID = \(Settings.cityID)")
            print("validData = \(validateData())")
            if (validateData() && !Settings.needUpdate()){
                print("Dataok")
                myApp.showTabBar()
                print("goToTabVC")
            } else {
                print("delete4all + Fetch")
                DAO.deleteOldData(.kDataAll)
                DAO.loadData(.kDataAll)
                NSUserDefaults.standardUserDefaults().removeObjectForKey("LastUpdate")
                NSUserDefaults .standardUserDefaults().setObject(NSDate(), forKey:"LastUpdate")
                NSUserDefaults.standardUserDefaults().synchronize()
            }
            
        }else{
                DAO.deleteOldData(.kDataAll)
                DAO.getCityList()
                print("GettingSityList")
        }
    }
    
    func validateData() -> Bool {
        if (Settings.japMenu.count > 0 && Settings.panMenu.count  > 0 && Settings.itaMenu.count > 0 && Settings.offersArray.count > 0)
        {
            print(Settings.japMenu.count)
            print(Settings.panMenu.count)
            print(Settings.itaMenu.count)
            print(Settings.offersArray.count)
            NSNotificationCenter.defaultCenter().removeObserver(self, name:Settings.kNetworkManagerDidEnd, object: nil)
      //      prv.progressView.removeFromSuperview()
            return true
        }else{
            return false
        }
    }
    
    func progressim( ) {
             refresh()
        if (progress < 1.0) {
           progress += 0.0759
            if (progress >= 1){ progress = 1}
     //       prv.progressView.progress = CGFloat(progress)
        }
    }
    
}


