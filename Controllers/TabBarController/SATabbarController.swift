//
//  SATabbarController.swift
//  SakuraReload
//
//  Created by Alexey.IOS on 01.09.15.
//  Copyright (c) 2015 SpbDev. All rights reserved.
//

import UIKit
import CoreData
import AlecrimCoreData

class SATabbarController: UITabBarController {

   // var bageView = PriceBageView.instanceFromNib() as! PriceBageView
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NSNotificationCenter.defaultCenter().addObserver (self, selector:"updateBager" , name:Settings.kUpdateBager, object: nil)
        
        configureView()
    
        // Do any additional setup after loading the view.
    }

    override func viewDidAppear(animated: Bool) {
      
        super.viewDidAppear(true)
        
        UIView.animateWithDuration(0.2, delay: 1, options: UIViewAnimationOptions.CurveEaseIn, animations: {[unowned self] in
            self.updateBager()
            }, completion:nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func configureView(){
        showBage()
        for item in self.tabBar.items as [UITabBarItem]! {
            item.image = item.image?.imageWithRenderingMode(UIImageRenderingMode.AlwaysOriginal)
        }
        self.selectedIndex = 2
    }
    
    func updateBager() {
            
//            var totalSum = 0
//            let tabArray = self.tabBar.items as NSArray!
//            let tabItem = tabArray.objectAtIndex(0) as! UITabBarItem
//            var c = dataContext.orders.toArray().count
//            print(c)
//            if (c == 0) {
//                
//                UIView.animateWithDuration(0.2, delay: 0.0, options: UIViewAnimationOptions.CurveLinear, animations: {[unowned self] in
//                    tabItem.setTitlePositionAdjustment(UIOffsetMake(25,0))
//                    self.bageView.dismiss()
//                    }, completion:nil)
//            }
//            var pArr = [NSNumber]()
//            var pricearray = [(NSNumber?,NSNumber?)]()
//            for order in dataContext.orders.toArray() {
//                pricearray.append((order.dishId,order.count))
//            }
//            for (dishId,count) in pricearray{
//                if let pcount = count {
//                    if let dish = dataContext.dishes.first({$0.dishId == dishId}){
//                        if let totalP = dish.price1 {
//                            var resultpr = totalP.integerValue * pcount.integerValue
//                            totalSum += resultpr
//                        }
//                    }
//                }
//                
//                let value = String(format:"%d р.", totalSum)
//                UIView.animateWithDuration(0.2, delay: 0.0, options: UIViewAnimationOptions.CurveLinear, animations: {[unowned self] in
//                    tabItem.setTitlePositionAdjustment(UIOffsetMake(-5,0))
//                    self.bageView.alpha = 1
//                    self.bageView.priceLabel.text = value
//                    }, completion:nil)
//            }
        }
    
    func showBage() {
//            let minx = UIApplication.sharedApplication().keyWindow!.frame.minX + 95
//            let maxy = UIApplication.sharedApplication().keyWindow!.frame.maxY - 25
//            bageView.center = CGPointMake(minx, maxy)
//            bageView.alpha = 0
//            self.view.addSubview(bageView)
        }
        
}
