//
//  Settings.swift
//  SakuraReload
//
//  Created by Alexey.IOS on 06.07.15.
//  Copyright (c) 2015 SpbDev. All rights reserved.
//

import Foundation
import AlecrimCoreData

struct Settings {
    
    static var cityID:NSNumber {
        get { if let cID = NSUserDefaults.standardUserDefaults().objectForKey("CityID") as? NSNumber { return cID }
        return 0
        }
        set { self.cityID = NSNumber()}
    }
    static var japMenu:NSArray {
        get {if let jm = (dataContext.kitchenCatalogs.filter{$0.menuType == "jap_menu"}.orderBy{$0.name}).toArray() as NSArray? {return jm}
            return []
            }
        }
    static var panMenu:NSArray {
        get {if let pm = (dataContext.kitchenCatalogs.filter{$0.menuType == "pan_menu"}.orderBy{$0.name}).toArray() as NSArray? {return pm}
            return []
            }
        }
    static var itaMenu:NSArray {
        get {if let im = (dataContext.kitchenCatalogs.filter{$0.menuType == "ital_menu"}.orderBy{$0.name}).toArray() as NSArray? {return im}
            return []
            }
    }
    static var offersArray:NSArray {
        get {if let offarr = dataContext.offers.toArray() as NSArray? {return offarr}
            return []
        }
    }
    static var cityesArray:NSArray {
        get {if let cityarr = dataContext.cities.toArray() as NSArray? {return cityarr}
            return []
        }
    }
    static var ABOUTSTRING:String {
        get {if let aboutString = "http://4588888.ru/api.php?action=about_info&city=\(cityID)" as String? {return aboutString}
            return "http://4588888.ru/api.php?action=about_info&city=2"
        }
    }
    static var lastUpdate:NSDate {
        get {
        if let lastUpd = NSUserDefaults.standardUserDefaults().objectForKey("LastUpdate") as? NSDate {
            return lastUpd
            }
        return NSDate()
        }
    }

    static func needUpdate() -> Bool {
        
        let currentDate = NSDate()
        if (hoursBetweenUpdates(Settings.lastUpdate, date2:currentDate) > 24 ){
        NSUserDefaults.standardUserDefaults().removeObjectForKey("LastUpdate")
        NSUserDefaults .standardUserDefaults().setObject(currentDate, forKey:"LastUpdate")
        NSUserDefaults.standardUserDefaults().synchronize()
            return true
        }
        return false
    }

    static func hoursBetweenUpdates(date1:NSDate, date2:NSDate) -> Int {
        _ = NSDate()
        _ = NSCalendar(identifier: NSCalendarIdentifierGregorian)
        let components = NSCalendar.currentCalendar().components(.Hour, fromDate: date1, toDate:date2, options:[])
        return components.hour
    }
    
    


static let PHONE_NUMBER:String = "4588888"
static let ORDER_URL:String = "http://4588888.ru/api.php?action=ordering_v2"
static let BASE_URL:String = "http://4588888.ru/api.php?action="
static let DELIVERY:String = "http://4588888.ru/api.php?action=delivery_info&city=2"
static let VACANCY:String = "http://4588888.ru/api.php?action=job_info&city=2"
static let TAMADA:String = "http://4588888.ru/api.php?action=competitions_info&city=2"
    
// API KEYS
static let googleMapsKey = "AIzaSyD8-OfZ21X2QLS1xLzu1CLCfPVmGtch7lo"

//API URLS
static let CITIESURL = "\(BASE_URL)\(CITIES)"
static let CITIES:String = "cities"
static let OFFERS:String = "offers&city="
static let JAPMENU:String = "jap_menu&city="
static let ITAMENU:String = "ital_menu&city="
static let PANMENU:String = "pan_menu&city="
static let JAPCAT:String = "jap_catalog&city="
static let ITACAT:String = "ital_catalog&city="
static let PANCAT:String = "pan_catalog&city="
static let PIZZATOP:String = "pizza_toppings&city="
static let BASENOODL:String = "base_noodles&city="
static let BASESAUS:String = "base_sauces&city="
static let BASETOP:String = "base_toppings&city="
static let BASEADDSAUS:String = "base_add_souces&city="
static let BASEMEAT:String = "base_meat&city="
static let NEWS:String = "news&city="
static let baseURLString = "https://maps.googleapis.com/maps/api/place/autocomplete/json"

static let itaMenuUrl = "\(BASE_URL)\(ITAMENU)\(cityID)"
static let japMenuUrl = "\(BASE_URL)\(JAPMENU)\(cityID)"
static let panMenuUrl = "\(BASE_URL)\(PANMENU)\(cityID)"
static let offerString = "\(BASE_URL)\(OFFERS)\(cityID)"
static let japCatUrl = "\(BASE_URL)\(JAPCAT)\(cityID)"
static let itaCatUrl = "\(BASE_URL)\(ITACAT)\(cityID)"
static let panCatUrl = "\(BASE_URL)\(PANCAT)\(cityID)"
static let pizzaTopUrl = "\(BASE_URL)\(PIZZATOP)\(cityID)"
static let kbaseNoodlUrl = "\(BASE_URL)\(BASENOODL)\(cityID)"
static let kBASESAUSlUrl = "\(BASE_URL)\(BASESAUS)\(cityID)"
static let kBASETOPUrl = "\(BASE_URL)\(BASETOP)\(cityID)"
static let kBASEADDSAUSUrl = "\(BASE_URL)\(BASEADDSAUS)\(cityID)"
static let kBASEMEATUrl = "\(BASE_URL)\(BASEMEAT)\(cityID)"
static let kNEWSUrl = "\(BASE_URL)\(NEWS)\(cityID)"
    
//Notifications
static let kNetworkManagerDidEnd = "NetworkManagerDidEnd"
static let kNetworkManagerDidEndWithError = "NetworkManagerDidEndWithError"
static let kCityListDone = "CityListDone"
static let kCityListChanged = "CityListChanged"
static let kOrderCreated = "NewOrderCreated"
static let kUpdateBager = "UpdateBager"
}

//Settings
enum Size: Int {
case kSizeIsNotPizza = 0, kSizeDiam30, kSizeDiam40
}

enum Kitchen: Int {
  case kKitchenMixed = 0,
    kKitchenItalianOnly,
    kKitchenJapanOnly
}

enum Action: Int {
   case kLabelAction = 1,
    kLabelNew,
    kLabelPepper,
    kLabelVeg,
    kLabelHit
}

enum DataType: Int {
    
   case kDataAll = 0,
    kDataCities,
    kDataMenuList,
    kDataJapMenu,
    kDataItaMenu,
    kDataPanMenu,
    kDataJapCat,
    kDataItalCat,
    kDataPanCat,
    kDataPizzaTop,
    kDataBaseNodl,
    kDataBaseSaus,
    kDataBaseTop,
    kDataBaseAddSous,
    kDataBaseMeat,
    kDataNews,
    kDataOrders,
    kDataOffers
}

// Extensions
extension UIView {
    class func loadFromNibNamed(nibNamed: String, bundle : NSBundle? = nil) -> UIView? {
        return UINib(
            nibName: nibNamed,
            bundle: bundle
            ).instantiateWithOwner(nil, options: nil)[0] as? UIView
    }

    func rotate360Degrees(duration: CFTimeInterval = 1.0, completionDelegate: AnyObject? = nil) {
        let rotateAnimation = CABasicAnimation(keyPath: "transform.rotation")
        rotateAnimation.fromValue = 0.0
        rotateAnimation.toValue = CGFloat(M_PI * 2.0)
        rotateAnimation.duration = duration
        
        if let delegate: AnyObject = completionDelegate {
            rotateAnimation.delegate = delegate
        }
        self.layer.addAnimation(rotateAnimation, forKey: nil)
    }

}


extension UIImage{
    
    class func roundedRectImageFromImage(image:UIImage,imageSize:CGSize,cornerRadius:CGFloat)->UIImage{
        UIGraphicsBeginImageContextWithOptions(imageSize,false,0.0)
        let bounds=CGRect(origin: CGPointZero, size: imageSize)
        UIBezierPath(roundedRect: bounds, cornerRadius: cornerRadius).addClip()
        image.drawInRect(bounds)
        let finalImage=UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return finalImage
    }
    
}

typealias ColorTuple = (red: CGFloat, green: CGFloat, blue: CGFloat, alpha: CGFloat)

extension UIColor {
    class func randomFlatColor() -> UIColor {
        struct RandomColors {
            static let colors: Array<ColorTuple> = [
                (red: 0.9, green: 0.0, blue: 0.0, alpha: 0.7),  // red
                (red: 0.0, green: 0.7, blue: 0.0, alpha: 0.7),  // green
                (red: 0.0, green: 0.0, blue: 0.9, alpha: 0.7)   // blue
                // etc...
            ]
          
        }
        
        let colorCount = UInt32(RandomColors.colors.count)
        let randomIndex = arc4random_uniform(colorCount)
        let color = RandomColors.colors[Int(randomIndex)]
        
        return UIColor(red: color.red, green: color.green, blue: color.blue, alpha: color.alpha)
    }
}

// Helper Methods
func clearFromHtml(htmlString:NSString?) -> String {
    if (htmlString != nil){
    var clearedString = htmlString!.stringByReplacingOccurrencesOfString("&quot;", withString: "\"")
    clearedString = clearedString.stringByReplacingOccurrencesOfString("&amp;", withString: "&")
    clearedString = clearedString.stringByReplacingOccurrencesOfString("&nbsp;", withString: "")
    clearedString = clearedString.stringByReplacingOccurrencesOfString("\r\n", withString: "")
    clearedString = clearedString.stringByReplacingOccurrencesOfString("&laquo;", withString: "\"")
    clearedString = clearedString.stringByReplacingOccurrencesOfString("&raquo;", withString: "\"")
    clearedString = clearedString.stringByReplacingOccurrencesOfString("&rsquo;", withString: "")
    clearedString = clearedString.stringByReplacingOccurrencesOfString("&#65279;", withString: "")
    clearedString = clearedString.stringByReplacingOccurrencesOfString("&lt;/br&gt;", withString: "")
    clearedString = clearedString.stringByReplacingOccurrencesOfString("&lt;b&gt;", withString: "")
    clearedString = clearedString.stringByReplacingOccurrencesOfString("&lt;/b&gt;", withString: "")
    clearedString = clearedString.stringByReplacingOccurrencesOfString("&lt;br&gt;", withString: "")
    clearedString = clearedString.stringByReplacingOccurrencesOfString("&ndash; ", withString: "")
    clearedString = clearedString.stringByReplacingOccurrencesOfString("&hellip; ", withString:"...")
    clearedString = clearedString.stringByReplacingOccurrencesOfString("&middot;", withString: "-")
    clearedString = clearedString.stringByReplacingOccurrencesOfString("&bull;", withString: "-")
    return clearedString;
    }
    return ""
}

//func textToImage(drawText: NSString, inImage: UIImage, atPoint:CGPoint)->UIImage{
//    
//    // Setup the font specific variables
//    let textColor: UIColor = UIColor.whiteColor()
//    let textFont: UIFont = UIFont(name: "Helvetica Bold", size: 12)!
//    
//    //Setup the image context using the passed image.
//    UIGraphicsBeginImageContext(inImage.size)
//    let textFontAttributes = [
//        NSFontAttributeName: textFont,
//        NSForegroundColorAttributeName: textColor,
//    ]
//    inImage.drawInRect(CGRectMake(0, 0, inImage.size.width, inImage.size.height))
//    let rect: CGRect = CGRectMake(atPoint.x, atPoint.y, inImage.size.width, inImage.size.height).
//    drawText.drawInRect(rect, withAttributes: textFontAttributes)
//    let newImage: UIImage = UIGraphicsGetImageFromCurrentImageContext()
//    UIGraphicsEndImageContext()
//    return newImage
//    
//}

enum SegueIdentifier: String {
    case ListCollection = "ListCollection"
    case BoxCreate = "BoxCreate"
    case SearchResults = "SearchResults"
}






