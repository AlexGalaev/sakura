//
//  NoodlesMapper.swift
//  SakuraReload
//
//  Created by Alexey.IOS on 15.07.15.
//  Copyright (c) 2015 SpbDev. All rights reserved.
//

import ObjectMapper

class CityMapper: Mappable {
    var status: NSString?
    var cities: [MCity]?
    required init?(_ map: Map) {
        mapping(map)
    }
    func mapping(map: Map) {
        status          <- map["status"]
        cities          <- map["cities"]
    }
}

class MCity: Mappable {
    var cityId: NSString?
    var name: String?
    var phone: String?
    required init?(_ map: Map) {
        mapping(map)
    }
    func mapping(map: Map) {
        cityId      <- map["id"]
        phone       <- map["phone"]
        name        <- map["name"]
    }
}

class DishCat: Mappable {
    var status: NSString?
    var action: NSString?
    var japcatalog: [MDish]?
    var pancatalog: [MDish]?
    var itacatalog: [MDish]?
    required init?(_ map: Map) {
        mapping(map)
    }
    func mapping(map: Map) {
        status          <- map["status"]
        action          <- map["action"]
        japcatalog      <- map["jap_catalog"]
        pancatalog      <- map["pan_catalog"]
        itacatalog      <- map["ital_catalog"]
    }
    
}

class MDish: Mappable {
    var categoryId: NSString?
    var dishId: NSString?
    var infoLabel: NSNumber?
    var ingredients: String?
    var lPhoto: String?
    var name:   String?
    var price1: NSString?
    var price2: NSNumber?
    var sPhoto: String?
    var size1: NSNumber?
    var size2: NSNumber?
    var sortedId: Double?
    var weight1: String?
    var weight2: String?
    required init?(_ map: Map) {
        mapping(map)
    }
    func mapping(map: Map) {
        categoryId          <- map["category_id"]
        dishId              <- map["id"]
        infoLabel           <- map["info_label"]
        ingredients         <- map["ingredients"]
        name                <- map["name"]
        price1              <- map["price"]
        sPhoto              <- map["s_photo"]
        lPhoto              <- map["l_photo"]
    }
}

class KitchenCatMapper: Mappable {
    var japmenu: [Menu]?
    var itmenu: [Menu]?
    var panmenu: [Menu]?
    var status: NSString?
    var action: NSString?
    required init?(_ map: Map) {
        mapping(map)
    }
    func mapping(map: Map) {
        status      <- map["status"]
        action      <- map["action"]
        japmenu     <- map["jap_menu"]
        itmenu      <- map["ital_menu"]
        panmenu     <- map["pan_menu"]
    }
}

struct Menu:Mappable {
    var menuId: NSString?
    var name:   String?
    var photo:  String?
    init(){}
    init?(_ map: Map) {
        mapping(map)
    }
    mutating func mapping(map: Map) {
        menuId          <- map["id"]
        name            <- map["name"]
        photo           <- map["photo"]
    }
}
class OfferMapper: Mappable {
    var status: NSString?
    var action: NSString?
    var offers: [MOffer]?
    required init?(_ map: Map) {
        mapping(map)
    }
    func mapping(map: Map) {
        status <- map["status"]
        action <- map["action"]
        offers <- map["offers"]
    }
}

class MOffer: Mappable {
    var date: NSString?
    var ldescr: NSString?
    var lphoto: NSString?
    var name: NSString?
    var offer_id: NSNumber?
    var s_photo: NSString?
    var sdescr: NSString?
    
    required init?(_ map: Map) {
        mapping(map)
    }
    // Mappable
    func mapping(map: Map) {
        date            <- map["date"]
        ldescr          <- map["l_descr"]
        lphoto          <- map["l_photo"]
        name            <- map["name"]
        offer_id        <- map["id_offers"]
        s_photo         <- map["s_photo"]
        sdescr          <- map["s_descr"]
    }
}
struct PizzaToppings: Mappable {
    var pizzatoppings: [PizaTop]?
    var status: NSString?
    var action: NSString?
    init(){}
    init?(_ map: Map) {
        mapping(map)
    }
    mutating func mapping(map: Map) {
        status          <- map["status"]
        action          <- map["action"]
        pizzatoppings   <- map["pizza_toppings"]
    }
}

struct PizaTop:Mappable {
    
    var pizatopId: NSString?
    var name: NSString?
    var price: NSString?
    var weight: NSString?
    init(){}
    init?(_ map: Map) {
        mapping(map)
    }
    mutating func mapping(map: Map) {
        pizatopId       <- map["id"]
        name            <- map["name"]
        price           <- map["price"]
        weight          <- map["weight"]
    }
}
struct BaseNoodles: Mappable {
    var noodles: [Noodles]?
    var status: NSString?
    var action: NSString?
    var price: NSString?
    init(){}
    init?(_ map: Map) {
        mapping(map)
    }
    mutating func mapping(map: Map) {
        status          <- map["status"]
        action          <- map["action"]
        noodles         <- map["base_noodles"]
        price           <- map["price"]
    }
}
struct Noodles:Mappable {
    var name: NSString?
    var bnID: NSString?
    init(){}
    init?(_ map: Map) {
        mapping(map)
    }
    mutating func mapping(map: Map) {
        name            <- map["name"]
        bnID            <- map["id"]
    }
}
struct BaseSauses: Mappable {
    var sauses: [Saus]?
    var status: NSString?
    var action: NSString?
    init(){}
    init?(_ map: Map) {
        mapping(map)
    }
    mutating func mapping(map: Map) {
        status          <- map["status"]
        action          <- map["action"]
        sauses         <- map["base_sauces"]
    }
}
struct Saus:Mappable {
    var name: NSString?
    var saID: NSString?
    init(){}
    init?(_ map: Map) {
        mapping(map)
    }
    mutating func mapping(map: Map) {
        name            <- map["name"]
        saID            <- map["id"]
    }
}

struct BaseToppings: Mappable {
    var topings: [BTopings]?
    var status: NSString?
    var action: NSString?
    init(){}
    init?(_ map: Map) {
        mapping(map)
    }
    mutating func mapping(map: Map) {
        status          <- map["status"]
        action          <- map["action"]
        topings         <- map["base_toppings"]
    }
}
struct BTopings:Mappable {
    var name: NSString?
    var tID: NSString?
    var price: NSString?
    init(){}
    init?(_ map: Map) {
        mapping(map)
    }
    mutating func mapping(map: Map) {
        name            <- map["name"]
        tID             <- map["id"]
        price           <- map["price"]
    }
}

struct BaseAddSouses: Mappable {
    var addsss: [BSss]?
    var status: NSString?
    var action: NSString?
    init(){}
    init?(_ map: Map) {
        mapping(map)
    }
    mutating func mapping(map: Map) {
        status          <- map["status"]
        action          <- map["action"]
        addsss         <- map["base_add_souces"]
    }
}
struct BSss:Mappable {
    var name: NSString?
    var tID: NSString?
    var price: NSString?
    
    init(){}
    init?(_ map: Map) {
        mapping(map)
    }
    mutating func mapping(map: Map) {
        name            <- map["name"]
        tID             <- map["id"]
        price           <- map["price"]
    }
}





struct BaseAddMeat: Mappable {
    var addsss: [Meat]?
    var status: NSString?
    var action: NSString?
    init(){}
    init?(_ map: Map) {
        mapping(map)
    }
    mutating func mapping(map: Map) {
        status          <- map["status"]
        action          <- map["action"]
        addsss         <- map["base_meat"]
    }
}
struct Meat:Mappable {
    var name: NSString?
    var tID: NSString?
    var price: NSString?
    
    init(){}
    init?(_ map: Map) {
        mapping(map)
    }
    mutating func mapping(map: Map) {
        name            <- map["name"]
        tID             <- map["id"]
        price           <- map["price"]
    }
}

class NewsMapper: Mappable {
    var status: NSString?
    var news: [MNews]?
    required init?(_ map: Map) {
        mapping(map)
    }
    func mapping(map: Map) {
        status <- map["status"]
        news <- map["news"]
    }
}
class MNews: Mappable {
    var date: NSString?
    var ldescr: NSString?
    var lphoto: NSString?
    var name: NSString?
    var offer_id: NSNumber?
    var s_photo: NSString?
    var sdescr: NSString?
    
    required init?(_ map: Map) {
        mapping(map)
    }
    // Mappable
    func mapping(map: Map) {
        date            <- map["date"]
        ldescr          <- map["l_descr"]
        lphoto          <- map["l_photo"]
        name            <- map["name"]
        offer_id        <- map["id_offers"]
        s_photo         <- map["s_photo"]
        sdescr          <- map["s_descr"]
    }
}









