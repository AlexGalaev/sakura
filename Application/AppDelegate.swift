//
//  AppDelegate.swift
//  Sakura
//
//  Created by Alexey.IOS on 14.10.15.
//  Copyright © 2015 AlexeyGalaev. All rights reserved.
//

import UIKit
import AlecrimCoreData
import ReachabilitySwift

let reachability = Reachability.reachabilityForInternetConnection()!
let dataContext = DataContext()
let DAO = NetworkManager()
let navController = UINavigationController()
let story = UIStoryboard(name: "Main", bundle: nil)
let myApp = AppDelegate()
var tabBar:SATabbarController!

@UIApplicationMain

class AppDelegate: UIResponder, UIApplicationDelegate, UIViewControllerTransitioningDelegate {
    
    var window: UIWindow?
    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
//        UIGraphicsBeginImageContext(self.window!.frame.size)
//        var image: UIImage = UIImage(named:"background-logo.png")!
//        image.drawAsPatternInRect(self.window!.bounds)
//        image = UIGraphicsGetImageFromCurrentImageContext()
//        UIGraphicsEndImageContext()
//        self.window!.backgroundColor = UIColor(patternImage:image)
        let vc = story.instantiateViewControllerWithIdentifier("SplashVC") as! SplashVC
        self.window!.rootViewController = navController
        navController.addChildViewController(vc)
        reachability.startNotifier()
        checkReachability()
        self.window?.makeKeyAndVisible()
        setupAppearance()
        return true
    }
    
    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
        
    }
    
    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        
    }
    
    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(application: UIApplication) {
        print(reachability.isReachable())
        if (reachability.isReachable()){
            if(navController.topViewController!.isKindOfClass(NoNetworkVC)){
                showSplashVC()
            } else {
                print("ActivedoNothing")
            }
        }
    }
    func applicationWillTerminate(application: UIApplication) {
        NSNotificationCenter.defaultCenter().removeObserver(self, name:Settings.kCityListChanged, object: nil)
        //   reachability.stopNotifier()
        //  NSNotificationCenter.defaultCenter().removeObserver(self, name: ReachabilityChangedNotification, object: nil)
        //  navController.viewControllers = nil
    }
    
    func showSplashVC(){
        navController.popToRootViewControllerAnimated(true)
    }
    
    func showCityList(){
        //let sitylist = story.instantiateViewControllerWithIdentifier("CityListVC") as! CityListVC
        // navController.pushViewController(sitylist, animated: true)
    }
    
    func showNoNetwork(){
        if(navController.topViewController!.isKindOfClass(NoNetworkVC)){
            print(navController.topViewController)
        } else {
            let nonetvc = story.instantiateViewControllerWithIdentifier("noNetworkVC")
            navController.pushViewController(nonetvc, animated:true)
        }
    }
    
    func showTabBar(){
        
        tabBar = story.instantiateViewControllerWithIdentifier("tabBarController") as! SATabbarController
        navController.pushViewController(tabBar, animated: true)
    }
    
    func checkReachability() {
        reachability.whenReachable = { reachability in
            if reachability.isReachableViaWiFi() {
                print("Reachable via WiFi")
                if(navController.topViewController!.isKindOfClass(NoNetworkVC)){
                    print("doNothingcheckReachability")
                    self.showSplashVC()
                }
                print("doNothingcheckReachability")
            } else {
                if(navController.topViewController!.isKindOfClass(NoNetworkVC)){
                    print("Reachable via Cellular")
                    self.showSplashVC()
                }
                print("doNothingcheckReachability")
            }
        }
        reachability.whenUnreachable = { reachability in
            print("Not reachable")
            self.showNoNetwork()
        }
    }
    
    func setupAppearance() {
        
        UITextField.appearance().keyboardAppearance = UIKeyboardAppearance.Dark
        let navarrbgImage = UIImage(named:"navogation-bar-logo.png")
        UINavigationBar.appearance().setBackgroundImage(navarrbgImage, forBarMetrics:.Default)
        UINavigationBar.appearance().tintColor = UIColor.whiteColor()
        UITabBarItem.appearance().setTitleTextAttributes([NSForegroundColorAttributeName: UIColor.whiteColor()], forState:.Normal)
        UITabBar.appearance().shadowImage = UIImage()
        UITabBar.appearance().backgroundImage = UIImage(named:"tab-bar-logo.png")
        UISearchBar.appearance().backgroundImage = UIImage(named:"search_bar_background")
        let tabBarItemFont = UIFont(name: "a_AvanteBs" , size: 10) ?? UIFont.systemFontOfSize(10)
        UITabBarItem.appearance().setTitleTextAttributes([NSFontAttributeName: tabBarItemFont,  NSForegroundColorAttributeName:UIColor.whiteColor()], forState: UIControlState.Normal)
        
    }
}

